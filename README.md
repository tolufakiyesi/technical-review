# Technical Review Solution

This solution implements a file upload and cache service.
Stores images uploaded on cloudinary. The app is deployed on heroku

## Setup

- Create and activate virtual environemnt

```
$ python -m venv ./env
$ source env/bin/activate
```

- Install requirements

```
$ pip install -r requirements.txt
```

- Add cloudinary keys to environmental variables
```
$ export CLOUDNAME=#######
$ export APIKEY=###########
$ export APISECRET=##########
```

- Start the app locally

```
$ export FLASK_APP=index.py
$ flask run
```

## Usage

Please find the link to the postman collection to test with
https://www.getpostman.com/collections/c56da10f15cbaf971041


## Who to talk to
- [Tolu Fakiyesi](mailto://tolufakiyesi@yahoo.com)