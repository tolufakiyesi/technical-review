from flask import Flask, request
from .lib import ImageUploader
import os
from werkzeug import secure_filename

app = Flask(__name__)
UPLOAD_FOLDER = './uploads' 

helper=ImageUploader()

@app.route('/', methods=['POST'])
def home():
    try:        
        uploadfile = request.files['file']
        if uploadfile:
            filename = secure_filename(uploadfile.filename)
            uploadfile.save(os.path.join(UPLOAD_FOLDER,filename))
        output = helper.upload('uploads/'+filename)
        return 'Uploaded! '+str(output)
    except Exception as e:
        return 'An exception occured: '+str(e)
    