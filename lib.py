from . import ImageUtil
import os
from PIL import Image
import cloudinary
import cloudinary.uploader

class ImageUploader():

    def __init__(self):
        # self.dimension = [0,0]
        self.width = 0
        self.height = 0
        self.imagecache = {}

        cloudinary.config( 
            cloud_name = os.getenv('CLOUDNAME'), 
            api_key = os.getenv('APIKEY'), 
            api_secret = os.getenv('APISECRET') 
        )

    def cacheImageInMemory(self, imageFile, outputFile):
        self.imagecache[imageFile] = outputFile
        return

    def uploadToServer(self, imageFile):
        return cloudinary.uploader.upload(imageFile)['url']

    def upload(self, imageFileName):
        test = {}
        test["maxWidth"] = 1200
        test["maxHeight"] = 1200
        
        if imageFileName in self.imagecache:
            # check if image has been saved to cache memory
            return self.imagecache[imageFileName]

        with Image.open(imageFileName) as imageFile:

            if not self.validate(imageFile, test):
                return

            outputFilename = self.uploadToServer(imageFileName)
            self.cacheImageInMemory(imageFileName, outputFilename)
            
            return outputFilename

    def validate(self, imageFile, test):
        # imageFile needs to be valid
        if imageFile is None:
            raise Exception("imageFile is null")
        
        self.width = ImageUtil.getWidth(imageFile)
        self.height = ImageUtil.getHeight(imageFile)

        maxWidth = test.get("maxWidth") # maxWidth
        maxHeight = test.get("maxHeight") # maxHeight

        if self.width < 1 or self.width > maxWidth:
            raise Exception("Invalid width")
        
        if self.height < 1 or self.height > maxHeight:
            raise Exception("Invalid height")            


        return True